<?php

namespace Database\Seeders;

use App\Models\ZkChannel;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [];
        for ($i = 1; $i <= 10; $i++){
            $data[] = [
                "user_id" => $faker->randomDigitNotZero(),
                "name" => $faker->title,
                "description" => $faker->text,
                "image" => $faker->url,
                "is_deleted" => $faker->boolean,
                "is_archive" => $faker->boolean,
                "subscribers" => $faker->randomDigit()
            ];
        }
        ZkChannel::insert($data);
    }
}
