<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zk_topics', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description');
            $table->bigInteger('zk_channel_id');
            $table->boolean('is_deleted')->default(false);
            $table->boolean('is_archive')->default(false);
            $table->tinyInteger('comments_active')->default(2);
            $table->integer('likes')->default(0);
            $table->integer('comments_count')->default(0);
            $table->bigInteger('repost_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zk_topics');
    }
};
