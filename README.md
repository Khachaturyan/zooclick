#for installing...

Clone this repo

````
git clone https://gitlab.com/Khachaturyan/zooclick
````


install all dependencies
``composer install``

copy .env.example as .env


setup your db connection credentials in .env
````
DB_CONNECTION=yourConnection
DB_HOST=yourHost
DB_PORT=yourPort
DB_DATABASE=yourDatabaseName
DB_USERNAME=yourDatabaseUserName
DB_PASSWORD=yourPassword
````

to migrate db tables run command

```
php artisan migrate
```

for seeding fake data run

```
php artisan db:seed
``` 

for testing run
````
php artisan serv
```` 

Swagger docs in path 
``"/api/documentation"``
