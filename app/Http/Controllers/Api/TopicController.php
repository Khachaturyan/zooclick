<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ZkTopic;
use App\Services\LikeService;
use App\Services\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'zk_channel_id' => 'required|exists:zk_channels,id',
            'comments_active' => 'in:banned,everyone,subscribers',
            'images' => 'required|array',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages(), 403);
        }

        $topic = Topic::Create($request->all());

        if (!$topic) {
            return $this->getFailResponse('something went wrong');
        }
        return $this->getSuccessResponse('done', $topic, 201);
    }

    public function index(Request $request)
    {
        $perPage = 10;
        if (isset($request->per_page)) {
            $perPage = $request->per_page;
        }
        $data = ZkTopic::paginate($perPage);
        return $this->getSuccessResponse('ok', $data);
    }

    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'zk_topic_id' => 'required',
            'like' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages(), 403);
        }

        $data = $request->all();

        $likes = LikeService::reaction($data);
        if ($likes['error']) {
            return $this->getFailResponse($likes['message']);
        }

        return $this->getSuccessResponse([],[],204);
    }
}
