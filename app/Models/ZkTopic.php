<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZkTopic extends Model
{
    use HasFactory;

    const COMMENT_ACTIVE_BANNED = 1;
    const COMMENT_ACTIVE_EVERYONE = 2;
    const COMMENT_ACTIVE_SUBSCRIBERS = 3;

    protected $fillable = [
        "name",
        "description",
        "zk_channel_id",
        "is_archive",
        "is_deleted",
        "comments_active",
        "likes",
        "comments_count"
    ];

    protected $with = ['topicImages'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function topicImages()
    {
        return $this->hasMany(ZkTopicImages::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zkChannel()
    {
        return $this->belongsTo(ZkChannel::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getCommentsActiveAttribute($value)
    {
        switch ($value) {
            case self::COMMENT_ACTIVE_BANNED:
                return 'banned';
                break;
            case self::COMMENT_ACTIVE_EVERYONE:
                return 'everyone';
                break;
            case self::COMMENT_ACTIVE_SUBSCRIBERS:
                return 'subscribers';
                break;
            default:
                return $value;
                break;
        }
    }

    /**
     * @param $value
     */
    public function setCommentsActiveAttribute($value)
    {
        switch ($value) {
            case 'banned':
                $this->attributes['comments_active'] = self::COMMENT_ACTIVE_BANNED;
                break;
            case 'everyone':
                $this->attributes['comments_active'] = self::COMMENT_ACTIVE_EVERYONE;
                break;
            case 'subscribers':
                $this->attributes['comments_active'] = self::COMMENT_ACTIVE_SUBSCRIBERS;
                break;
            default:
                $this->attributes['comments_active'] = $value;
                break;
        }
    }

}
