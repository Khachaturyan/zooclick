<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZkTopicImages extends Model
{
    use HasFactory;

    protected $fillable = ['zk_topic_id','image'];

    public function zkTopic(){
        $this->belongsTo(ZkTopic::class);
    }
}
