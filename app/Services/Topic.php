<?php

namespace App\Services;

use App\Models\ZkTopic;

class Topic
{

    /**
     * @param array $data
     * @return false
     */
    public static function Create(array $data)
    {
        try {
            $topic = ZkTopic::create([
                'name' => $data['name'],
                'description' => $data['description'],
                'zk_channel_id' => $data['zk_channel_id'],
                'comments_active' => $data['comments_active']
            ]);
            $topicImage = TopicImage::upload($data['images'], $topic->id);
            if (!$topicImage) {
                return false;
            }

            return $topic->load('topicImages');
        } catch (\Exception $e) {
            return false;
        }

    }
}
