<?php

namespace App\Services;

use App\Models\ZkTopicImages;

class TopicImage
{

    /**
     * @param array $images
     * @param int $topic_id
     * @return bool
     */
    public static function upload(array $images, int $topic_id):bool
    {
        try {
            foreach ($images as $image) {

                $imageName = time() . $image->getClientOriginalName();
                $image->move(storage_path('images'), $imageName);

                ZkTopicImages::create([
                    'zk_topic_id' => $topic_id,
                    'image' => "images/{$imageName}"
                ]);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }


    }
}
