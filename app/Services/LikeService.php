<?php

namespace App\Services;

use App\Models\ZkLikes;
use App\Models\ZkTopic;

class LikeService
{
    public static function reaction($data)
    {
        try {
            $likes = ZkLikes::where('user_id', 1)//сюда ид юзера типа Auth::user()->id
                ->where('zk_topic_id', $data['zk_topic_id']);

            if ($likes->get()->count() > 0 && $data['like']) {
                return [
                    'error' => true,
                    'message' => 'user already like this topic'
                ];
            }

            if (!$likes->get()->count() > 0 && !$data['like']) {
                return [
                    'error' => true,
                    'message' => 'user dont like this topic'
                ];
            }

            $topic = ZkTopic::find($data['zk_topic_id']);
            if ($data['like']) {
                ZkLikes::create([
                    'user_id' => 1,//сюда ид юзера типа Auth::user()->id
                    'zk_topic_id' => $topic->id
                ]);
                $topic->likes++;
            } else {
                $likes->delete();
                $topic->likes--;
            }

            $topic->save();

            return [
                'error' => false,
                'message' => 'done'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => 'something went wrong'
            ];
        }

    }
}
